local Library = require "CoronaLibrary"

-- Create library
local lib = Library:new{ name='plugin.library', publisherId='com.yogome.appboy' }

-------------------------------------------------------------------------------
-- BEGIN (Insert your implementation starting here)
-------------------------------------------------------------------------------

-- This sample implements the following Lua:
-- 
--    local PLUGIN_NAME = require "plugin_PLUGIN_NAME"
--    PLUGIN_NAME.test()
--    
lib.init = function()
	native.showAlert( 'init', 'plugin.library.init() invoked', { 'OK' } )
	print( 'init' )
end

lib.show = function()
	native.showAlert( 'show', 'plugin.library.show() invoked', { 'OK' } )
	print( 'show' )
end

-------------------------------------------------------------------------------
-- END
-------------------------------------------------------------------------------

-- Return an instance
return lib
